package com.poc.security.client.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@Configuration
@EnableResourceServer // This is to access resources within auth server if any
                      // if we comment this we will not be able to access resources from this auth server even if we pass correct token
public class ResourceServerConfig {

}
