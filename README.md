## auth server client

### Filter Chain

```fc
[
  org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter@68b734a8, 
  org.springframework.security.web.context.SecurityContextPersistenceFilter@2e62ead7, 
  org.springframework.security.web.header.HeaderWriterFilter@17fede14, 
  org.springframework.security.web.authentication.logout.LogoutFilter@f0a66bd, 
  **** org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationProcessingFilter@16132f21, 
  org.springframework.security.web.savedrequest.RequestCacheAwareFilter@bf75b5c, 
  org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter@32faa16c, 
  org.springframework.security.web.authentication.AnonymousAuthenticationFilter@1a464fa3, 
  org.springframework.security.web.session.SessionManagementFilter@2e43c38d, 
  org.springframework.security.web.access.ExceptionTranslationFilter@1624775, 
  org.springframework.security.web.access.intercept.FilterSecurityInterceptor@2ffe243f
]

```

### Flow

```f
OAuth2AuthenticationProcessingFilter -> OAuth2AuthenticationManager -> RemoteTokenServices
```